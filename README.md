# PEC 2 - Juego de plataformas

## Super West Land. Basado en Super Mario Bros

Este videojuego se ha diseñado con la intención de representar uno de los niveles del mítico videojuego Super Mario Bros de la consola NES, en concreto, el nivel 1-1. En este nivel se
presentan las mecánicas básicas de las que dispone el jugador al tener el control del personaje, y algunas de las características que permiten al jugador potenciar a su personaje.

### Objetivo

El propósito principal de esta práctica es profundizar en el uso de físicas, aplicadas tanto a los enemigos, como al propio protagonista en un entorno en dos dimensiones. El jugador tendrá
que ir avanzando en el nivel hasta alcanzar la bandera, que implicará la finalización del mismo.

### Estructura

El proyecto se ha organizado de manera que, a través de las diferentes escenas, junto con sus correspondientes scripts, se logre una experiencia satisfactoria en el jugador.

Para comenzar, se ha diseñado una primera escena **(Home)**, en la que muestra al jugador el menú habitual en la gran mayoría de videojuegos, donde se le da la posibilidad tanto de comenzar una nueva
partida, como de salir del propio videojuego.

A continuación, una pantalla **(Intermediate)** enseña al jugador las diferentes estadísticas que puede obtener a lo largo del nivel, así como el número de vidas restantes del personaje principal.

- El número de puntos obtenido al matar a los enemigos, al destruir las cajas que contienen monedas u obtener un PowerUp (mejora de personaje, con la que resiste dos golpes de enemigos) 
  o LifeUp (una seta que representa que el jugador recibe una vida más).
- El número de monedas alcanzadas a lo largo del nivel.
- El nivel que está jugando en ese momento el jugador.
- El tiempo restante para llegar a la meta.

En esta pantalla, se ha usado un script para la representación de los diferentes datos, así como la contabilización de las vidas, para que una vez que el jugador llege a 0, automáticamente
pierda la partida. Cada vez que se accede a esta escena, los datos son reiniciados, con la intención de que el jugador al volver a consumir la siguiente vida, comience con los datos
reseteados.

La pantalla del propio nivel, llamada **(Level)** se ha diseñado con la máxima semejanza posible con respecto al videojuego original, a excepción de los sprites. Al igual que se explica
anteriormente, en esta pantalla se observan todas las estadísticas en la parte superior, así como la zona jugable en el resto de la misma.
Para la estructuración del mapa, se ha usado la funcionalidad de los "TileMaps", disponibles por defecto en Unity, para el suelo y los diferentes obstáculos, a excepción de los bloques, el personaje
y el fondo.

Para esta escena, han sido necesarios varios scripts, de los cuales los más relevantes son:

- *PlayerMovement.cs*

    En este script se ha diseñado todo el movimiento referente al personaje principal, junto con sus animaciones,  así como ciertas colisiones con diferentes objetos repartidos por 
    el mapa (enemigos, cajas, etc.).
    
- *EnemyController.cs*

    En este fichero se ha implementado el movimiento de los enemigos dispuestos en el mapa, así como sus colisiones con el entorno y el jugador.
    
- *BoxHitCoin.cs* / *BoxHitPowerUp.cs*

    La manera en la que interactúa el jugador con los distintos bloques se han diseñado en estos ficheros, con la intención de aislar el funcionamiento de ambas.
    Por un lado, en el primer script, se contemplan tanto los bloques que contienen monedas, como los normales. Por otro lado, en el segundo se ha tenido en cuenta solo la que contiene
    el PowerUp.

Por último, una vez que el jugador haya consumido las 3 vidas, o haya finalizado el nivel con éxito, se mostrará al jugador una escena final **(End)**, en la que se le informará 
si ha conseguido la victoria, o por el contrario, no ha conseguido llegar al final.
    
### Enlace al vídeo

https://youtu.be/2t56RUFVEZs


### Futuras implementaciones/mejoras

- Arreglar movimiento referente a enemigos en plataformas.
- Fallo al obtener PowerUp e ir contra los enemigos. Se muere automáticamente y no da posibilidad de coger otra.
- Salto más alto de lo normal a veces.

### Creador: Samuel Valcárcel Arce
