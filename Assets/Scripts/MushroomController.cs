﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase que mueve ambas mejoras. Hay que corregirlo
public class MushroomController : MonoBehaviour
{
    public float speed = 1f;
    public float maxSpeed = 1f;
    public Rigidbody2D rigidBody;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rigidBody.AddForce(Vector2.right * speed);
        float limitSpeed = Mathf.Clamp(rigidBody.velocity.x, -maxSpeed, maxSpeed);
        rigidBody.velocity = new Vector2(limitSpeed, rigidBody.velocity.y);

        // Se produce el rebote de manera instantánea
        if (rigidBody.velocity.x > -0.01f && rigidBody.velocity.x < 0.01f)
        {
            speed = -speed;
            rigidBody.velocity = new Vector2(speed, rigidBody.velocity.y);
        }
    }
}
