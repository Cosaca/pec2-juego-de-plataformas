﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Clase que muestra los datos de la parte superior
public class Gameplay : MonoBehaviour
{
    public Text textTime;
    public Text pointsPlayerText;
    public Text coinsPlayerText;
    public float timeGame;

    private void Start ()
    {
        timeGame = 400;
    }

    private void FixedUpdate ()
    {
        timeGame -= Time.deltaTime;
        textTime.text = timeGame.ToString("###");
        coinsPlayerText.text = StoreData.coinsPlayer.ToString();
        pointsPlayerText.text = StoreData.pointsPlayer.ToString();
    }
}
