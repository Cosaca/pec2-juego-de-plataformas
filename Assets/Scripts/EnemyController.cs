﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase que controla el comportamiento de los enemigos
public class EnemyController : MonoBehaviour
{
    public float speed = 1f;
    public float maxSpeed = 10f;

    private Rigidbody2D rigidBody;

    private void Start ()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate ()
    {
        rigidBody.AddForce(Vector2.right * speed);
        float limitSpeed = Mathf.Clamp(rigidBody.velocity.x, -maxSpeed, maxSpeed);
        rigidBody.velocity = new Vector2(limitSpeed, rigidBody.velocity.y);

        // Se produce el rebote con objetos de manera instantánea
        if (rigidBody.velocity.x > -0.1f && rigidBody.velocity.x < 0.1f)
        {
            speed = -speed;
            rigidBody.velocity = new Vector2(speed, rigidBody.velocity.y);
        }

        // Cambiar de lado
        if (rigidBody.velocity.x < -0.01f)
        {
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
        }
        if (rigidBody.velocity.x > 0.01f)
        {
            transform.localScale = new Vector3(-0.8f, 0.8f, 0.8f);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Floor"))
        {
            Destroy(gameObject);
        }
    }

    // Metodo que evita que los enemigos caigan al vacio
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("EnemyCollision"))
        {
            speed = -speed;
            rigidBody.velocity = new Vector2(speed, rigidBody.velocity.y);
        }
    }
}
