﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase que gestiona el comportamiento del bloque de PowerUp
public class BoxHitPowerUp : MonoBehaviour {

    private bool hasCollide;
    private Transform boxItem;
    public GameObject powerUp;

    private void Start()
    {
        boxItem = GetComponent<Transform>();
        hasCollide = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player") && hasCollide)
        {
            hasCollide = false;
            boxItem.transform.parent.Translate(0, 20 * Time.deltaTime, 0);
            Instantiate(powerUp, boxItem.transform.parent.position + new Vector3(0, 1, 0), boxItem.transform.parent.rotation);
            StartCoroutine(StopBoxMovement());
        }
    }

    private IEnumerator StopBoxMovement()
    {
        yield return new WaitForSeconds(0.1f);
        boxItem.transform.parent.Translate(0, -23 * Time.deltaTime, 0);
        boxItem.GetComponentInParent<Renderer>().material.color = new Color32(190, 190, 190, 255);
    }
}
