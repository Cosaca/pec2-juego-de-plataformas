﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

// Clase usada en la escena intermedia cada vez que el jugador pierde una vida
public class LoadingScene : MonoBehaviour
{
    public Text lifesNumber;

    private void Start()
    {
        StoreData.DeleteData();

        lifesNumber.text = StoreData.remainingLifes.ToString();
        StartCoroutine(SceneLoadingAfterDelay());
    }

    private IEnumerator SceneLoadingAfterDelay()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Level", LoadSceneMode.Single);
    }
}
