﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase relacionada con la cámara del jugador
public class CameraFollower : MonoBehaviour {

    public Transform player;
    public Vector2 minCameraPos, maxCameraPos;

    private void FixedUpdate()
    {
        float playerPosX = player.transform.position.x;
        float playerPosY = player.transform.position.y;

        transform.position = new Vector3(Mathf.Clamp(playerPosX, minCameraPos.x, maxCameraPos.x), Mathf.Clamp(playerPosY, minCameraPos.y, maxCameraPos.y), transform.position.z);
    }

}
