﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase que gestiona el comportamiento del bloque de LifeUp
public class BoxHitLifeUp : MonoBehaviour {

    private bool hasCollide;
    private Transform boxItem;
    public GameObject lifeUp;

    private void Start()
    {
        boxItem = GetComponent<Transform>();
        hasCollide = true;
        boxItem.parent.gameObject.GetComponentInParent<Renderer>().material.color = new Color32(255, 255, 255, 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player") && hasCollide)
        {
            hasCollide = false;
            boxItem.transform.parent.Translate(0, 20 * Time.deltaTime, 0);
            Instantiate(lifeUp, boxItem.transform.parent.position + new Vector3(0, 1, 0), boxItem.transform.parent.rotation);
            StartCoroutine(StopBoxMovement());
        }
    }

    private IEnumerator StopBoxMovement()
    {
        yield return new WaitForSeconds(0.1f);
        boxItem.transform.parent.Translate(0, -23 * Time.deltaTime, 0);
        boxItem.GetComponentInParent<Renderer>().material.color = new Color32(190, 190, 190, 255);
    }
}
