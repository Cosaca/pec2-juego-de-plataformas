﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Clase encargada de gestionar los menús
public class ManagerGame : MonoBehaviour
{
    public void NewGame()
    {
        SceneManager.LoadScene("Intermediate");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void Menu()
    {
        SceneManager.LoadScene("Home");
    }
}
