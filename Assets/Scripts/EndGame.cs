﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Clase usada para la escena final y sus textos
public class EndGame : MonoBehaviour
{
    public Text winnerText;
    public Text finalCoins;
    public Text finalPoints;

    private void Start ()
    {
        StoreData.remainingLifes = 3;
        finalCoins.text = StoreData.coinsPlayer.ToString();
        finalPoints.text = StoreData.pointsPlayer.ToString();
	    if(StoreData.playerWins)
        {
            winnerText.text = "¡Victoria!";
        }
        else
        {
            winnerText.text = "¡Derrota!";
        }
	}
}
