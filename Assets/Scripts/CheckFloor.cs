﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase que comprueba si el jugador está tocando el suelo
public class CheckFloor : MonoBehaviour
{
    private PlayerMovement player;
    public AudioClip deadSlime;

    private void Start ()
    {
        player = GetComponentInParent<PlayerMovement>();
	}

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
        {
            player.ground = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            player.ground = false;
        }
    }

    private IEnumerator OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("DeadEnemy"))
        {
            AudioSource.PlayClipAtPoint(deadSlime, collision.gameObject.transform.position);
            StoreData.pointsPlayer += 100;
            player.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 15, ForceMode2D.Impulse);
            yield return new WaitForSeconds(0.1f);
            Destroy(collision.gameObject);
        }
    }
}
