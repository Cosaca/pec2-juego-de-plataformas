﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Clase que gestiona el comportamiento de los bloques de monedas y normales
public class BoxHitCoin : MonoBehaviour
{ 
    private bool hasCollide;
    private Transform boxItem;
    public AudioClip coinSound;
    public AudioClip boxBrokenSound;

    private void Start ()
    {
        boxItem = GetComponent<Transform>();
        hasCollide = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player") && hasCollide)
        {
            if (boxItem.parent.Find("Coin"))
            {
                AudioSource.PlayClipAtPoint(coinSound, boxItem.transform.parent.Find("Coin").position);

                hasCollide = false;
                boxItem.transform.parent.Translate(0, 20 * Time.deltaTime, 0);
                boxItem.transform.parent.Find("Coin").Translate(0, 50 * Time.deltaTime, 0);
                StartCoroutine(StopBoxMovement());
                StartCoroutine(StopCoinMovement());

                StoreData.pointsPlayer += 100;
                StoreData.coinsPlayer += 1;
            }
            else
            {
                if(StoreData.powerUpUsed)
                {
                    Destroy(boxItem.parent.gameObject);
                    AudioSource.PlayClipAtPoint(boxBrokenSound, boxItem.parent.gameObject.transform.position);
                }
                else
                {
                    boxItem.transform.parent.Translate(0, 20 * Time.deltaTime, 0);
                    StartCoroutine(StopBoxMovement());
                }
            }
        }
    }

    private IEnumerator StopBoxMovement()
    {
        yield return new WaitForSeconds(0.1f);
        boxItem.transform.parent.Translate(0, -23 * Time.deltaTime, 0);
        boxItem.GetComponentInParent<Renderer>().material.color = new Color32(190, 190, 190, 255);
    }

    private IEnumerator StopCoinMovement()
    {
        yield return new WaitForSeconds(0.5f);
        Destroy(boxItem.parent.Find("Coin").gameObject);
    }
}
