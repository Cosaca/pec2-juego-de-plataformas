﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Clase que tiene el movimiento del jugador
public class PlayerMovement : MonoBehaviour
{
    public float speed = 5f;
    public float maxSpeed = 5f;
    public float jumpForce = 6.5f;
    public bool ground;
    public AudioClip powerUpSound, gameOverSound, winSound;
    
    private new Animator animation;
    private Rigidbody2D rigidBody;
    private bool jump;
    private bool hasDie = false;
    private bool hitEnemyWithPowerUp;
    //private int firstTimeWithPowerUp = 0;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animation = GetComponent<Animator>();
        hitEnemyWithPowerUp = false;
        animation.SetBool("Dead", false);
    }

    private void Update()
    {
        // Parametros de las animaciones
        animation.SetFloat("Speed", Mathf.Abs(rigidBody.velocity.x));
        animation.SetBool("Ground", ground);

        if (Input.GetKeyDown(KeyCode.Space) && ground)
        {
            jump = true;
        }
    }

    private void FixedUpdate()
    {
        // Arreglar deslizamiento en suelo de plataformas
        Vector3 fixedFrictionGround = rigidBody.velocity;
        fixedFrictionGround *= 0.75f;

        if (ground)
            rigidBody.velocity = fixedFrictionGround;

        float horizontalMove = Input.GetAxis("Horizontal");
        rigidBody.AddForce(Vector2.right * speed * horizontalMove);
        
        float limitSpeed = Mathf.Clamp(rigidBody.velocity.x, -maxSpeed, maxSpeed);
        rigidBody.velocity = new Vector2(limitSpeed, rigidBody.velocity.y);
        FlipCharacter(horizontalMove);

        if (jump)
        {
            rigidBody.velocity = Vector2.up * jumpForce;
            jump = false;
        }
    }

    // Metodo que da la vuelta al personaje en caso de cambiar de lado
    private void FlipCharacter(float horizontalMove)
    {
        FlipRight(horizontalMove);
        FlipLeft(horizontalMove);
    }

    private void FlipLeft(float horizontalMove)
    {
        if (horizontalMove < -0.1f)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            if (StoreData.powerUpUsed)
            {
                if (!hitEnemyWithPowerUp)
                {
                    transform.localScale = new Vector3(-1.3f, 1.3f, 1f);
                }
                else
                {
                    //firstTimeWithPowerUp++;
                    StoreData.powerUpUsed = false;
                    hasDie = false;
                    transform.localScale = new Vector3(-1f, 1f, 1f);
                }
            }
            else
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }
    }

    public void FlipRight(float horizontalMove)
    {
        if (horizontalMove > 0.1f)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            if (StoreData.powerUpUsed)
            {
                if (!hitEnemyWithPowerUp)
                {
                    transform.localScale = new Vector3(1.3f, 1.3f, 1f);
                }
                else
                {
                    //firstTimeWithPowerUp++;
                    StoreData.powerUpUsed = false;
                    hasDie = false;
                    transform.localScale = new Vector3(1f, 1f, 1f);
                }
            }
            else
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
            }
        }
    }

    // Detectar colision con objetos
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Floor") && !hasDie)
        {
            AudioSource.PlayClipAtPoint(gameOverSound, collision.gameObject.transform.position);
            StartCoroutine(PlayerInteraction(collision));
        }

        if (collision.collider.CompareTag("Enemy") && !hasDie)
        {
            if (StoreData.powerUpUsed)
            {
                hitEnemyWithPowerUp = true;
            }
            else
            {
                animation.SetBool("Dead", true);
                AudioSource.PlayClipAtPoint(gameOverSound, collision.gameObject.transform.position);
                StartCoroutine(PlayerInteraction(collision));
            }
        }

        if(collision.gameObject.CompareTag("PowerUp"))
        {
            StoreData.powerUpUsed = true;
            ItemInteraction(collision);
        }

        if (collision.gameObject.CompareTag("LifeUp"))
        {
            StoreData.remainingLifes += 1;
            ItemInteraction(collision);
        }

        if (collision.gameObject.CompareTag("Finish"))
        {
            StartCoroutine(Finish(collision));
        }
    }

    // Metodo que se usa al colisionar con mejoras
    private void ItemInteraction(Collision2D collision)
    {
        AudioSource.PlayClipAtPoint(powerUpSound, collision.gameObject.transform.position);
        StoreData.pointsPlayer += 1000;
        Destroy(collision.gameObject);
    }

    // Metodo que se usa al colisionar con enemigos
    private IEnumerator PlayerInteraction(Collision2D collision)
    {
        rigidBody.velocity = new Vector2(0, 0);
        enabled = false;
        StoreData.remainingLifes -= 1;
        hasDie = true;
        yield return new WaitForSeconds(1f);
        StoreData.Restart();
    }

    // Metodo que se usa al terminar el nivel
    private IEnumerator Finish(Collision2D collision)
    {
        StoreData.pointsPlayer += 5000;
        enabled = false;
        Camera.main.GetComponent<AudioSource>().enabled = false;
        AudioSource.PlayClipAtPoint(winSound, collision.gameObject.transform.position);
        yield return new WaitForSeconds(3f);
        StoreData.playerWins = true;
        SceneManager.LoadScene("End");
    }
}
