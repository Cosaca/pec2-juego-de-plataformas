﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Clase que almacena datos relacionados con la partida
public static class StoreData
{
    public static int remainingLifes = 3;
    public static int pointsPlayer;
    public static int coinsPlayer;
    public static bool growthPlayer;
    public static bool powerUpUsed = false;
    public static bool playerWins;

    public static void DeleteData()
    {
        powerUpUsed = false;
        pointsPlayer = 0;
        coinsPlayer = 0;
    }

    public static void Restart()
    {
        if (remainingLifes == 0)
        {
            playerWins = false;
            SceneManager.LoadScene("End");
        }
        else
        {
            SceneManager.LoadScene("Intermediate");
        }
    }
}
